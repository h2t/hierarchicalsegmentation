#!/usr/bin/python
from __future__ import division
import argparse
import glob
import os, sys
import ntpath
import Ice

import numpy as np
import pylab as pl
import matplotlib.pyplot as plt

import IceGrid
import IceGrid_Registry_ice
# import IceGrid_Admin_ice

from lxml import etree


__author__ = 'waechter'


def main():
    parser = argparse.ArgumentParser(description="Segmentation evaluation script that retrieves the segmentations for MMM Files.")
    parser.add_argument("--MMMFile", "-m", help='Path to the MMMFile or to directory with MMMFiles', default="", dest="MMMFile")
    parser.add_argument("rest", nargs=argparse.REMAINDER)
    parser.add_argument("--IceConfigFile", default="/home/waechter/.armarx/default.cfg", dest="iceConfig")


    args = parser.parse_args()

    properties = Ice.createProperties(sys.argv)
    properties.load(args.iceConfig)

    init_data = Ice.InitializationData()
    init_data.properties = properties

    ic = Ice.initialize(init_data)
    # registry = ic.stringToProxy("IceGrid/Registry")
    # registry = IceGrid.RegistryPrx.checkedCast(registry)
    #
    # assert isinstance(registry, IceGrid_Registry_ice.Registry)
    # admin = registry.createAdminSession("bla", "bla")
    # print admin.getAdmin().getAllApplicationNames()
    # sys.exit(0)

    Ice.loadSlice('-I%s %s -DMININTERFACE' % (Ice.getSliceDir(), '/home/waechter/projects/HierarchicalSegmentation/source/HierarchicalSegmentation/interface/core/SemanticEventChainInterfaces.ice'))
    import hierarchicalsegmentation

    motions = get_MMM_Files(args.MMMFile)
    motions = sorted(motions)
    i = 1
    for motion in motions:
        print "%d/%d: %s" % (i, len(motions), ntpath.basename(motion))
        read_semantic_segmentation_ice_with_default(motion, args.iceConfig)
        # getFFT(motion)
        i += 1



def getFFT(MMMFile, iceConfig):
    properties = Ice.createProperties(sys.argv)
    properties.load(iceConfig)

    init_data = Ice.InitializationData()
    init_data.properties = properties

    ic = Ice.initialize(init_data)

    Ice.loadSlice('-I%s %s -DMININTERFACE' % (Ice.getSliceDir(), '/home/waechter/projects/HierarchicalSegmentation/source/HierarchicalSegmentation/interface/core/SemanticEventChainInterfaces.ice'))
    import hierarchicalsegmentation


    baseProxy = ic.stringToProxy("MMMSECGenerator")
    semanticsegmenterproxy = hierarchicalsegmentation.SemanticEventChainGeneratorInterfacePrx.checkedCast(baseProxy)
    x = semanticsegmenterproxy.getXPositions(MMMFile, "rightHand")
    # print x
    p = np.fft.fft(x)
    f = np.linspace(0, 100, len(p))
    data = np.random.rand(301) - 0.5
    ps = np.abs(np.fft.fft(x))**2
    # print ps
    size = data.size
    # print size
    time_step = 1/100
    print ps
    freqs = np.fft.fftfreq(size, time_step)
    idx = np.argsort(freqs)
    # print idx
    # print freqs
    plt.plot(freqs[idx], ps[idx])
    plt.savefig("/tmp/plot.png")

def read_semantic_segmentation_ice_with_default(MMMFile, ice_config):
    Ice.loadSlice('-I%s %s -DMININTERFACE' % (Ice.getSliceDir(), '/home/waechter/projects/HierarchicalSegmentation/source/HierarchicalSegmentation/interface/core/SemanticEventChainInterfaces.ice'))
    import hierarchicalsegmentation
    params = []
    for i in range(0,hierarchicalsegmentation.SemanticConfig.eSemanticConfigParamCount.value):
        params.append(0)
    params[hierarchicalsegmentation.SemanticConfig.eDistanceThreshold.value] = 5.48
    params[hierarchicalsegmentation.SemanticConfig.eSemanticMergeThreshold.value] = 38
    params[hierarchicalsegmentation.SemanticConfig.eHystereseFactor.value] = 1.89

    params[hierarchicalsegmentation.SemanticConfig.eMinSegmentSize.value] = 1.071
    params[hierarchicalsegmentation.SemanticConfig.eWindowSize.value] = 0.86
    params[hierarchicalsegmentation.SemanticConfig.eDivideThreshold.value] = 657
    params[hierarchicalsegmentation.SemanticConfig.eDerivation.value] = 1.83
    params[hierarchicalsegmentation.SemanticConfig.eWeightRoot.value] = 4.02
    params[hierarchicalsegmentation.SemanticConfig.eGaussFilterWidth.value] = 0.08
    return read_semantic_segmentation_ice(MMMFile, params, ice_config)

def read_semantic_segmentation_ice(MMMFile, params, ice_config):
    properties = Ice.createProperties(sys.argv)
    properties.load(ice_config)

    init_data = Ice.InitializationData()
    init_data.properties = properties

    ic = Ice.initialize(init_data)
    # registry = ic.stringToProxy("IceGrid/Registry")
    # registry = IceGrid.RegistryPrx.checkedCast(registry)
    #
    # assert isinstance(registry, IceGrid_Registry_ice.Registry)
    # admin = registry.createAdminSession("bla", "bla")
    # print admin.getAdmin().getAllApplicationNames()
    # sys.exit(0)
    Ice.loadSlice('-I%s %s -DMININTERFACE' % (Ice.getSliceDir(), '/home/waechter/projects/HierarchicalSegmentation/source/HierarchicalSegmentation/interface/core/SemanticEventChainInterfaces.ice'))
    import hierarchicalsegmentation
    multiresult = {}

    baseProxy = ic.stringToProxy("MMMSECGenerator")
    semanticsegmenterproxy = hierarchicalsegmentation.SemanticEventChainGeneratorInterfacePrx.checkedCast(baseProxy)
    keyframes = semanticsegmenterproxy.calcKeyframesWithConfig(MMMFile, params)
    # print keyframes
    multiresult["Hierarchical Segmentation"] = keyframes

    keyframesXML = get_keyframe_xml(multiresult)
    # print keyframesXML

    filesplit = os.path.splitext(MMMFile)

    keyframeFile = filesplit[0] + ".kfxml"
    doc = etree.fromstring(keyframesXML)

    tree = etree.ElementTree(doc)
    tree.write(keyframeFile, pretty_print=True)


def read_multi_semantic_segmentation_ice(MMMFiles, params, ice_config = None):



    baseProxy = ic.stringToProxy("MMMSECGenerator")

    semanticsegmenterproxy = hierarchicalsegmentation.SemanticEventChainGeneratorInterfacePrx.checkedCast(baseProxy)
    # multimultiresult = []
    async_ptr_list = []
    kfFileList = []
    i = 0
    for m in MMMFiles:
        multiresult = {}
        async_ptr_list.append(semanticsegmenterproxy.begin_calcKeyframesWithConfig(m, params))
    for result_ptr in async_ptr_list:
        keyframes = semanticsegmenterproxy.end_calcKeyframesWithConfig(result_ptr)
        multiresult["Hierarchical Segmentation"] = keyframes

        keyframesXML = get_keyframe_xml(multiresult)
        # print keyframesXML

        filesplit = os.path.splitext(MMMFiles[i])
        i+=1
        keyframeFile = filesplit[0] + ".kfxml"
        doc = etree.fromstring(keyframesXML)

        tree = etree.ElementTree(doc)
        tree.write(keyframeFile, pretty_print=True)
        kfFileList.append(keyframeFile)
        # print keyframes




        # baseProxy = ic.stringToProxy("MultiSegmenter")
        # multisegmenterproxy = hierarchicalsegmentation.MultiSegmenterInterfacePrx.checkedCast(baseProxy)
        # multiresult.update(multisegmenterproxy.segment(MMMFile, "rightHand"))
        #
        # print multiresult
        #
        # multimultiresult.append(multiresult)
    # print kfFileList
    return kfFileList




def get_keyframe_xml(multiresult):
    root = etree.Element("root")
    for algo in multiresult:
        algoXML = etree.SubElement(root, "algorithm", name=algo)
        keyframes = multiresult[algo]
        for object in keyframes:
            objectXML = etree.SubElement(algoXML, "object", name=object)
            for frame in keyframes[object]:
                frameString = "%f" % frame
                etree.SubElement(objectXML, "frame").text = frameString
    return etree.tostring(root, encoding='utf8', method='xml',  pretty_print=True)


def get_MMM_Files(motionFilePath):
    motions = []
    if os.path.isfile(motionFilePath):
        # print "Single file: " + motionFilePath
        motions.append(motionFilePath)
    else:
        motions = glob.glob(motionFilePath + "/*.xml")
        # print "Batch mode: "
        # print motions

    return motions

#
# root = etree.Element("root")
#
# objectXML = etree.SubElement(root, "object", name="RedCup")
# etree.SubElement(objectXML, "frame").text = "123"
# # tree = ET.ElementTree(root)
# print etree.tostring(root, pretty_print=True)
# tree = etree.ElementTree(root)
# tree.write("/tmp/myxml.xml", pretty_print=True)


if __name__ == "__main__":
    main()