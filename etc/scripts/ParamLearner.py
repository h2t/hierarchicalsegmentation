#!/usr/bin/python
__author__ = 'waechter'


import argparse
import Segmenter
import SegmentationEvaluator
import Ice
import ntpath

import array
import random

import numpy

from deap import algorithms
from deap import base
from deap import creator
from deap import tools


Ice.loadSlice('-I%s %s -DMININTERFACE' % (Ice.getSliceDir(), '/home/waechter/projects/HierarchicalSegmentation/source/HierarchicalSegmentation/interface/core/SemanticEventChainInterfaces.ice'))
import hierarchicalsegmentation

def getInvididual(mutatationProb = -1):
    params = creator.Individual()

    for i in range(0,hierarchicalsegmentation.SemanticConfig.eSemanticConfigParamCount.value):
        params.append(0)
    params[hierarchicalsegmentation.SemanticConfig.eDistanceThreshold.value] = 6.81
    params[hierarchicalsegmentation.SemanticConfig.eSemanticMergeThreshold.value] = 47
    params[hierarchicalsegmentation.SemanticConfig.eHystereseFactor.value] = 1.72

    params[hierarchicalsegmentation.SemanticConfig.eMinSegmentSize.value] = 1.0974
    params[hierarchicalsegmentation.SemanticConfig.eWindowSize.value] = 0.54
    params[hierarchicalsegmentation.SemanticConfig.eDivideThreshold.value] = 819
    params[hierarchicalsegmentation.SemanticConfig.eDerivation.value] = 1.22
    params[hierarchicalsegmentation.SemanticConfig.eWeightRoot.value] = 3.756
    params[hierarchicalsegmentation.SemanticConfig.eGaussFilterWidth.value] = 0.078
    if random.random() <= mutatationProb:
        params = mutate(params)
        for i in range(3):
            params = mutate(params[0])
        return params[0]
    return params


parser = argparse.ArgumentParser(description="Learn Params of a segmentation algorithm with a genetic algorithm")
parser.add_argument("--MMMFile", "-m", help='Path to the MMMFile or to directory with MMMFiles', default="", dest="MMMFile")
parser.add_argument("rest", nargs=argparse.REMAINDER)
parser.add_argument("--IceConfigFile", default="/home/waechter/.armarx/default.cfg", dest="iceConfig")
parser.add_argument("--Algo", "-a", default="Hierarchical Segmentation", dest="algo")
parser.add_argument("--mainObjectName", "-o", help='Name of the mainobject for the segmentation', required=True)

args = parser.parse_args()

def getFitness(params):
    print "Params: ", params
    args = parser.parse_args()
    motions = Segmenter.get_MMM_Files(args.MMMFile)
    motions = sorted(motions)
    i = 1
    results = Segmenter.read_multi_semantic_segmentation_ice(motions, params, args.iceConfig)
    # for motion in motions:
    #     # print "%d/%d: %s" % (i, len(motions), ntpath.basename(motion))
    #     Segmenter.read_semantic_segmentation_ice(motion, params, args.iceConfig)
    #     # getFFT(motion)
    #     i += 1


    resultMap = SegmentationEvaluator.evaluateMotionFiles(args.MMMFile, args.mainObjectName)
    return (resultMap[args.algo][0],)


creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", list, fitness=creator.FitnessMin)

toolbox = base.Toolbox()

# Attribute generator


# Structure initializers
toolbox.register("individual", getInvididual)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)


def drawNewValue(value, positive = True, beInteger = False, sigma = 0.1) :
    value *= 1-numpy.random.normal(0, sigma)
    if(positive and value < 0):
        value = 0
    if beInteger:
        value = int(value)
    return value

def mutate(params):
    params[hierarchicalsegmentation.SemanticConfig.eDistanceThreshold.value] = drawNewValue(params[hierarchicalsegmentation.SemanticConfig.eDistanceThreshold.value])
    params[hierarchicalsegmentation.SemanticConfig.eSemanticMergeThreshold.value] = drawNewValue(params[hierarchicalsegmentation.SemanticConfig.eSemanticMergeThreshold.value])
    params[hierarchicalsegmentation.SemanticConfig.eHystereseFactor.value] = drawNewValue(params[hierarchicalsegmentation.SemanticConfig.eHystereseFactor.value])

    params[hierarchicalsegmentation.SemanticConfig.eMinSegmentSize.value] = drawNewValue(params[hierarchicalsegmentation.SemanticConfig.eMinSegmentSize.value])
    params[hierarchicalsegmentation.SemanticConfig.eWindowSize.value] = drawNewValue(params[hierarchicalsegmentation.SemanticConfig.eWindowSize.value])
    params[hierarchicalsegmentation.SemanticConfig.eDivideThreshold.value] *= 1-numpy.random.normal(0, 0.25)
    params[hierarchicalsegmentation.SemanticConfig.eDerivation.value] = drawNewValue(params[hierarchicalsegmentation.SemanticConfig.eDerivation.value])
    params[hierarchicalsegmentation.SemanticConfig.eWeightRoot.value] = drawNewValue(params[hierarchicalsegmentation.SemanticConfig.eWeightRoot.value])
    params[hierarchicalsegmentation.SemanticConfig.eGaussFilterWidth.value] = drawNewValue(params[hierarchicalsegmentation.SemanticConfig.eGaussFilterWidth.value] )
    return (params,)

toolbox.register("evaluate", getFitness)
toolbox.register("mate", tools.cxTwoPoint)
toolbox.register("mutate", mutate)
toolbox.register("select", tools.selTournament, tournsize=3)

def main():



    random.seed(64)
    pop = []
    for i in range(25):
        pop.append(getInvididual(1))
    for i in range(5):
        pop.append(getInvididual(0))

    hof = tools.HallOfFame(3)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean)
    stats.register("std", numpy.std)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)

    pop, log = algorithms.eaSimple(pop, toolbox, cxpb=0.5, mutpb=0.6, ngen=100,
                                   stats=stats, halloffame=hof, verbose=True)

    return pop, log, hof

if __name__ == "__main__":
    print main()