/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/


#include "GnuPlotWrapper.h"
#include "Defines.h"
#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <boost/filesystem.hpp>
#include <fstream>
namespace  armarx {

    void GnuPlotWrapper::plotTogether(const std::vector<DVec> &data, const std::vector<std::string> &titles, const char *tmp_filename, bool show, std::string additionalplotCommand)
    {
        if(data.size() < 2)
            throw LocalException("Input dimensions must be atleast 2 to plot.");
        for(unsigned int i= 1; i < data.size(); i++)
        {
            char specificFilename[1024];
            sprintf(specificFilename, tmp_filename, i);

            writeGNUPlotFileWithXAxis(data[0], data[i], specificFilename);
        }
        if (show) {
            char scriptBuff[10240];
            std::fstream file;
            sprintf(scriptBuff, tmp_filename, 0);
            std::stringstream scriptfile;
            scriptfile << scriptBuff<< "s";
            boost::filesystem::path filepath(scriptfile.str());
            boost::filesystem::create_directories(filepath.remove_filename());
            file.open(scriptfile.str().c_str(), std::fstream::out);
            if(!file.is_open())
                throw LocalException("Could not open file: " + scriptfile.str());
            std::cout << " file:" << scriptfile.str() << std::endl;
            char buf[10240];
            std::stringstream plots;
            if(!additionalplotCommand.empty())
                plots <<  additionalplotCommand << "\n";
            plots << " plot " ;
            for(unsigned int i= 1; i < data.size(); i++)
            {
                char buf_filename[255];
                sprintf(buf_filename, tmp_filename, i);
                plots << "\"" << buf_filename << "\" with lines ls " << i;
                if(i < titles.size())
                    plots << " title '" << titles.at(i) << "'";
                if(i != data.size()-1)
                    plots << ", ";
            }

            file << plots.str();
            file.close();
            sprintf(buf, GNUPLOT_COMMAND_FROM_FILE, scriptfile.str().c_str());
            std::cout << buf<< std::endl;
            /* Bart: res never used, causes warning
            int res;
            res = */system(buf);
        }
    }

    void GnuPlotWrapper::writeGNUPlotFileWithXAxis(const DVec &x, const DVec &y, const char *filename)
    {
        boost::filesystem::path filepath(filename);
        boost::filesystem::create_directories(filepath.remove_filename());


        FILE *fp = fopen(filename, "w");
        if(!fp)
            throw LocalException("Could not open file '") << filename <<"'!";
        for(unsigned int i=0; i<x.size() && i < y.size(); ++i)
            fprintf(fp, "%lf\t%lf\n", x[i], y[i]);

        fclose(fp);
    }

}
