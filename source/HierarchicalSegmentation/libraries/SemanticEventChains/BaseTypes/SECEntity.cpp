/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "SECEntity.h"

namespace armarx {

    SECEntity::SECEntity()
    {
    }

} // namespace armarx


hierarchicalsegmentation::SECEntityBasePtr armarx::SECEntity::getParentClass(const Ice::Current &)
{
}

bool armarx::SECEntity::isEqual(const hierarchicalsegmentation::SECEntityBasePtr &, const Ice::Current &)
{
}

std::string armarx::SECEntity::getId(const Ice::Current &) const
{
}

void armarx::SECEntity::setId(const std::string &, const Ice::Current &)
{
}

std::string armarx::SECEntity::getName(const Ice::Current &) const
{
}

void armarx::SECEntity::setName(const std::string &, const Ice::Current &)
{
}

memoryx::EntityAttributeBasePtr armarx::SECEntity::getAttribute(const std::string &, const Ice::Current &) const
{
}

bool armarx::SECEntity::hasAttribute(const std::string &, const Ice::Current &) const
{
}

void armarx::SECEntity::putAttribute(const memoryx::EntityAttributeBasePtr &, const Ice::Current &)
{
}

void armarx::SECEntity::removeAttribute(const std::string &, const Ice::Current &)
{
}

memoryx::NameList armarx::SECEntity::getAttributeNames(const Ice::Current &) const
{
}

void armarx::SECEntity::serialize(const armarx::ObjectSerializerBasePtr &, const Ice::Current &) const
{
}

void armarx::SECEntity::deserialize(const armarx::ObjectSerializerBasePtr &, const Ice::Current &)
{
}
