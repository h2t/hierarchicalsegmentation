/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "RelationCalculatorContext.h"
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/SceneObjectSet.h>

#include <boost/foreach.hpp>

using namespace VirtualRobot;

namespace armarx {

    RelationCalculatorContext::RelationCalculatorContext()
    {
    }

    VirtualRobot::SceneObjectSetPtr RelationCalculatorContext::getSceneObjectSet(const std::string &objectName)
    {
        MotionDataMap::const_iterator it = motions.find(objectName);
        if(it == motions.end())
        {
            ARMARX_INFO << "Object not found: " << objectName;
            return VirtualRobot::SceneObjectSetPtr();
        }
        std::vector<RobotNodePtr> colModels1 = it->second.model->getRobotNodes();
        SceneObjectSetPtr sos(new SceneObjectSet());
        BOOST_FOREACH(RobotNodePtr colModel, colModels1)
        {
            if(colModel->getCollisionModel())
                sos->addSceneObject(colModel);
        }
        return sos;
    }

    RobotPtr RelationCalculatorContext::getModel(const std::string &objectName)
    {
        MotionDataMap::const_iterator it = motions.find(objectName);
        if(it == motions.end())
        {
            ARMARX_INFO << "Object not found: " << objectName;
            return RobotPtr();
        }
        return it->second.model;
    }

}
