/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "BehindRelationCalculator.h"

#include <VirtualRobot/MathTools.h>
#include <MemoryX/libraries/memorytypes/entity/SEC/SECRelation.h>



using namespace VirtualRobot;

namespace armarx {

    BehindRelationCalculator::BehindRelationCalculator(RelationCalculatorContextPtr context) :
        RelationCalculatorInterface(context)
    {
    }

    memoryx::SECRelationBasePtr BehindRelationCalculator::checkRelation()
    {
//        MMM::MotionPtr obj1Motion = context->getPrimaryObject().motion;
//        MMM::MotionPtr obj2Motion = context->getPrimaryObject().motion;

//        RobotPtr obj1Model = context->getPrimaryObject().model;
//        RobotPtr obj2Model = context->getSecondaryObject().model;

//        obj1Model->setGlobalPose(obj1Motion->getMotionFrame(frame)->getRootPose());
//        obj2Model->setGlobalPose(obj2Motion->getMotionFrame(frame)->getRootPose());

//        SceneObjectSetPtr sos = context->getSceneObjectSet(object1->getName());
//        SceneObjectSetPtr sos2 = context->getSceneObjectSet(object2->getName());

//        context->getCDMan()->addCollisionModelPair(sos, sos2);
        Eigen::Vector3f P1;
        Eigen::Vector3f P2;
        int trID1;
        int trID2;
        c()->getCDMan()->getDistance(P1, P2, trID1, trID2);
        float minDistanceObj1 = getMinDistanceFromAgent(c()->getPrimaryObject().sos);
        float maxDistanceObj2 = getMaxDistanceFromAgent(c()->getSecondaryObject().sos);
        Eigen::Vector3f agentPos = c()->getAgentPose()->toEigen().block<3,1>(0,3);
        float angle = fabs(MathTools::getAngle(P1 - agentPos, P2 - agentPos));

        if(minDistanceObj1 > maxDistanceObj2
                && angle * 180/M_PI < 30)
            return new memoryx::Relations::BehindOfRelation(c()->getPrimaryObject().objectClass, c()->getSecondaryObject().objectClass);
        else
            return NULL;
    }

    float BehindRelationCalculator::getMaxDistanceFromAgent(SceneObjectSetPtr sos)
    {
        Eigen::Vector3f agentPos = c()->getAgentPose()->toEigen().block<3,1>(0,3);
        float max = 0;
        for(size_t i = 0; i < sos->getSize(); i++)
        {
            BoundingBox bb = sos->getSceneObject(i)->getCollisionModel()->getBoundingBox();
            std::vector<Eigen::Vector3f> points;
            points.push_back(bb.getMax());
            points.push_back(bb.getMin());
            Eigen::Vector3f cur;
            cur.setZero();

            for (int x = 0; x < 2; ++x) {
                cur[0] = points[x][0];
                for (int y = 0; y < 2; ++y) {
                    cur[1] = points[x][1];
                    for (int z = 0; z < 2; ++z) {
                        cur[2] = points[x][2];
                        max = std::max(max, (cur-agentPos).norm());
                    }
                }

            }

        }

        return max;
    }

    float BehindRelationCalculator::getMinDistanceFromAgent(SceneObjectSetPtr sos)
    {
        Eigen::Vector3f agentPos = c()->getAgentPose()->toEigen().block<3,1>(0,3);
        float min = std::numeric_limits<float>::max();
        for(size_t i = 0; i < sos->getSize(); i++)
        {
            BoundingBox bb = sos->getSceneObject(i)->getCollisionModel()->getBoundingBox();
            std::vector<Eigen::Vector3f> points;
            points.push_back(bb.getMax());
            points.push_back(bb.getMin());
            Eigen::Vector3f cur;
            cur.setZero();

            for (int x = 0; x < 2; ++x) {
                cur[0] = points[x][0];
                for (int y = 0; y < 2; ++y) {
                    cur[1] = points[x][1];
                    for (int z = 0; z < 2; ++z) {
                        cur[2] = points[x][2];
                        min = std::min(min, (cur-agentPos).norm());
                    }
                }

            }

        }

        return min;
    }


} // namespace armarx


