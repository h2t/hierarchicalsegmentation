#ifndef _DEFINES_H_
#define _DEFINES_H_

#ifndef _WIN32
#define GNUPLOT_COMMAND "gnuplot -e \"plot %s \" -persistent"
#define GNUPLOT_COMMAND_FROM_FILE "gnuplot %s -persistent"
#define _TYPENAME typename
#define KILL_ALL_PLOT_WINDOWS system("killall gnuplot");
#else
#define GNUPLOT_COMMAND "pgnuplot -e \"plot %s \" -"
#define INFINITY std::numeric_limits<double>::infinity()
#define _TYPENAME 
#define KILL_ALL_PLOT_WINDOWS system("taskkill /FI \"IMAGENAME eq wgnuplot.exe\"");
#endif

/*
#ifndef _WIN32
	res = system("killall gnuplot");
#else
	res = system("taskkill /FI \"IMAGENAME eq wgnuplot.exe\"");
#endif
*/

#endif 
