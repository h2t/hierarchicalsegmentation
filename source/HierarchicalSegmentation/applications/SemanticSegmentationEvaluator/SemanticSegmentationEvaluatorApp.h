/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    HierarchicalSegmentation::application::SemanticSegmentationEvaluator
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_APPLICATION_HierarchicalSegmentation_SemanticSegmentationEvaluator_H
#define _ARMARX_APPLICATION_HierarchicalSegmentation_SemanticSegmentationEvaluator_H


#include <HierarchicalSegmentation/components/SemanticSegmentationEvaluator/SemanticSegmentationEvaluator.h>

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>


namespace armarx
{
    /**
     * @class SemanticSegmentationEvaluatorApp
     * @brief A brief description
     *
     * Detailed Description
     */
    class SemanticSegmentationEvaluatorApp :
        virtual public armarx::Application
    {
        /**
         * @see armarx::Application::setup()
         */
        void setup(const ManagedIceObjectRegistryInterfacePtr& registry,
                   Ice::PropertiesPtr properties)
        {
            registry->addObject( Component::create<SemanticSegmentationEvaluator>(properties) );
        }
    };
}

#endif
