/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    HierarchicalSegmentation::ArmarXObjects::SemanticEventChainInterpreter
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_HierarchicalSegmentation_SemanticEventChainInterpreter_H
#define _ARMARX_COMPONENT_HierarchicalSegmentation_SemanticEventChainInterpreter_H


#include <ArmarXCore/core/Component.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/interface/component/PriorKnowledgeInterface.h>
#include <MemoryX/interface/component/LongtermMemoryInterface.h>
#include <HierarchicalSegmentation/interface/core/SemanticEventChainInterfaces.h>
#include <dmp/representation/trajectory.h>
#include <dmp/segmentation/characteristicsegmentation.h>

namespace armarx
{
    /**
     * @class SemanticEventChainInterpreterPropertyDefinitions
     * @brief
     * @ingroup Components
     */
    class SemanticEventChainInterpreterPropertyDefinitions:
            public ComponentPropertyDefinitions
    {
    public:
        SemanticEventChainInterpreterPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("SECGeneratorName", "SemanticEventChainGenerator", "Name of the Semantic Event Chain Generator app");
            defineOptionalProperty<std::string>("ViconCSVFile", "", "Path to Vicon CSV file - absolute or relative to ArmarXDataPaths");
            defineOptionalProperty<std::string>("MMMMotionFile", "", "Path to MMM XML file - absolute or relative to ArmarXDataPaths");
            defineOptionalProperty<float>("OACMatchingThreshold", 0.8f, "Threshold above the matching score for an OAC must be to be recognized");
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @class SemanticEventChainInterpreter
     * @brief A brief description
     *
     * Detailed Description
     */
    class SemanticEventChainInterpreter :
        virtual public armarx::Component,
            virtual public hierarchicalsegmentation::SemanticEventChainInterpreterInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "SemanticEventChainInterpreter";
        }

        bool compareRelations(memoryx::SECRelationBasePtr relation1, memoryx::SECRelationBasePtr relation2) const;
        Ice::Float compareObjectRelations(memoryx::SECObjectRelationsBasePtr relationsSet, memoryx::SECObjectRelationsBasePtr relationsSubset, memoryx::SECRelationPairList &matchingRelations) const;
        Ice::Float matchOAC(memoryx::OacBasePtr oac, memoryx::SECObjectRelationsBasePtr worldStateBefore, memoryx::SECObjectRelationsBasePtr worldStateAfter, memoryx::ObjectClassList &involvedObjects) const;
        memoryx::OacBasePtr findOAC(memoryx::SECObjectRelationsBasePtr worldStateBefore, memoryx::SECObjectRelationsBasePtr worldStateAfter, memoryx::ObjectClassList &involvedObjects ) const;
        memoryx::SECMotionSegmentList findOACSequence(const memoryx::SECKeyFrameMap &secs, const Ice::Current &c = Ice::Current());
        memoryx::OacBaseList findUnobservableOACs(const memoryx::SECKeyFrameBasePtr &keyframe);

        double checkSideConstraints(memoryx::SECObjectRelationsBasePtr OACPreconditions, memoryx::SECObjectRelationsBasePtr OACEffects, memoryx::OacBasePtr oac, memoryx::SECRelationPairList matchingPreconditions, memoryx::SECRelationPairList matchingEffects, memoryx::ObjectClassList &involvedObjects) const;
    protected:
        void recognize();
        void getMarkerBasedSECs();
        void getMMMBasedSECs();
        std::set<double> getAllKeyFrames(const std::vector<DMP::CharacteristicSegmentationData> &vec);
        std::map<std::string, DMP::SampledTrajectoryV2> findActions(const std::map<std::string, DMP::SemanticSegmentData>& labeledData, const std::string& objectInContact);
        std::string loadFileContent(const std::string & filepath) const;
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

        memoryx::PriorKnowledgeInterfacePrx priorKnowledge;
        memoryx::LongtermMemoryInterfacePrx longtermMemory;
        memoryx::PersistentObjectClassSegmentBasePrx objClassSegment;
        hierarchicalsegmentation::SemanticEventChainGeneratorInterfacePrx secGenPrx;
//        hierarchicalsegmentation::SECtoPEMConverterInterfacePrx SECtoPEMConverterPrx;
        memoryx::OacBaseList oacDatabase;

        float OACMatchingThreshold;

    };
    typedef class IceInternal::Handle<SemanticEventChainInterpreter> SemanticEventChainInterpreterPtr;
}

#endif
