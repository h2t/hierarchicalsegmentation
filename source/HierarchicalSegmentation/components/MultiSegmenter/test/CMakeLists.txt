
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore MultiSegmenter)
 
armarx_add_test(MultiSegmenterTest MultiSegmenterTest.cpp "${LIBS}")