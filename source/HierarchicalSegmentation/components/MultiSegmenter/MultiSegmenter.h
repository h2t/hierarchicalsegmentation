/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    HierarchicalSegmentation::ArmarXObjects::MultiSegmenter
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_HierarchicalSegmentation_MultiSegmenter_H
#define _ARMARX_COMPONENT_HierarchicalSegmentation_MultiSegmenter_H


#include <ArmarXCore/core/Component.h>


#include <HierarchicalSegmentation/interface/core/SemanticEventChainInterfaces.h>

namespace Seg
{
    class Segmentation;
    typedef boost::shared_ptr<Segmentation> SegmentationPtr;

    class AbstractConfiguration;
}

namespace armarx
{
    /**
     * @class MultiSegmenterPropertyDefinitions
     * @brief
     * @ingroup Components
     */
    class MultiSegmenterPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        MultiSegmenterPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<float>("zvc.velocityepsilon", 0.1f,"Velocity threshold");
            defineOptionalProperty<int>("zvc.numZeroJoints", 3,"Number of joint that need to be below velocity threshold");
            defineOptionalProperty<float>("pca.alpha", 25000,"Threshold");
            defineOptionalProperty<int>("pca.reduceDimensionsTo", 2,"Number of dimension the pca will reduce the space to");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @class MultiSegmenter
     * @ingroup Components
     * @brief A brief description
     *
     *
     * Detailed Description
     */
    class MultiSegmenter :
        virtual public armarx::Component,
            public hierarchicalsegmentation::MultiSegmenterInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "MultiSegmenter";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();

        void baseConfigure(Seg::AbstractConfiguration* config) const;



        // MultiSegmenterInterface interface
    public:
        hierarchicalsegmentation::SegmentationResultList segment(const std::string &MMMFilePath, const std::string &, const Ice::Current &);
    };
}

#endif
