armarx_component_set_name("SemanticEventChainGenerator")

find_package(Simox QUIET)
find_package(Eigen3 QUIET)

armarx_build_if(Simox_FOUND "Simox not available")
armarx_build_if(Eigen3_FOUND "Eigen3 not available")

if (Eigen3_FOUND AND Simox_FOUND)
    include_directories(${Simox_INCLUDE_DIRS})
    include_directories(${Eigen3_INCLUDE_DIR})
endif()

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore HierarchicalSegmentationInterfaces SemanticEventChains ${Simox_LIBRARIES})

set(SOURCES SemanticEventChainGenerator.cpp
    ViconCSVParser.cpp
)
set(HEADERS SemanticEventChainGenerator.h
    ViconCSVParser.h
)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)
