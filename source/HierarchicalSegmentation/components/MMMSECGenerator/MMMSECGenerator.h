/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    HierarchicalSegmentation::ArmarXObjects::MMMSECGenerator
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_HierarchicalSegmentation_MMMSECGenerator_H
#define _ARMARX_COMPONENT_HierarchicalSegmentation_MMMSECGenerator_H


#include <ArmarXCore/core/Component.h>
#include <MemoryX/interface/component/PriorKnowledgeInterface.h>
#include <HierarchicalSegmentation/interface/core/SemanticEventChainInterfaces.h>


namespace MMM
{
    class Motion;
    typedef boost::shared_ptr<Motion> MotionPtr;
    typedef std::vector<MotionPtr> MotionList;
}

namespace DMP
{
    class SampledTrajectoryV2;
}

namespace armarx
{
    /**
     * @class MMMSECGeneratorPropertyDefinitions
     * @brief
     * @ingroup Components
     */
    class MMMSECGeneratorPropertyDefinitions:
            public ComponentPropertyDefinitions
    {
    public:
        MMMSECGeneratorPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            defineOptionalProperty<float>("FPS", 100.0f, "Framerate of the motion data");
        }
    };

    /**
     * @class MMMSECGenerator
     * @brief A brief description
     *
     * Detailed Description
     */
    class MMMSECGenerator :
        virtual public armarx::Component,
            virtual public hierarchicalsegmentation::SemanticEventChainGeneratorInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "MMMSECGenerator";
        }

        static void Plot(const std::vector<double>& keyframes, const DMP::SampledTrajectoryV2 & traj);

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

        memoryx::PersistentObjectClassSegmentBasePrx objectClassSegment;
        memoryx::PriorKnowledgeInterfacePrx priorKnowledge;

        static hierarchicalsegmentation::DoubleList GetDefaultConfig();

        // SemanticEventChainGeneratorInterface interface
    public:
        memoryx::SECKeyFrameMap calculateSEC(const hierarchicalsegmentation::StringList &MMMMotionXMLString, const Ice::Current &c);
        hierarchicalsegmentation::ObjectKeyframes calcKeyframes(const std::string &MMMFilePath, const Ice::Current &);


    public:
        hierarchicalsegmentation::Keyframes getXPositions(const std::string &MMMFile, const std::string &objectName, const Ice::Current &);


        hierarchicalsegmentation::ObjectKeyframes calcKeyframesWithConfig(const std::string &MMMFilePath, const hierarchicalsegmentation::DoubleList &params, const Ice::Current &c);
    private:
        MMM::MotionList getMotionFile(std::string filePath);
        RecursiveMutex motionsMutex;
        std::map<std::string, MMM::MotionList> motionFiles;
        float fps;

        // SemanticEventChainGeneratorInterface interface
    public:
        hierarchicalsegmentation::KeyframesWithWorldStateVec getWorldStates(const std::string &MMMFilePath, const std::string &objectName, const hierarchicalsegmentation::DoubleList &params, const Ice::Current &c = Ice::Current());
        hierarchicalsegmentation::KeyframesWithWorldStateVec convertToSimpleWorldStates(const memoryx::SECKeyFrameMap & keyframes);

        // SemanticEventChainGeneratorInterface interface
    public:
        hierarchicalsegmentation::KeyframesWithWorldStateVec calcWorldStatesAtFrames(const std::string &, const hierarchicalsegmentation::DoubleList &frames, const hierarchicalsegmentation::DoubleList &segParams, const Ice::Current &);
    };
}

#endif
