
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore MMMSECGenerator)
 
armarx_add_test(MMMSECGeneratorTest MMMSECGeneratorTest.cpp "${LIBS}")