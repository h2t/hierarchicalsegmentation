/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "SemanticSegmentSegmenter.h"

#include <dmp/io/MMMConverter.h>
#include <dmp/representation/dmp/basicdmp.h>

#define FPS 100

using namespace DMP;
using namespace MMM;
using namespace memoryx;

namespace armarx {


    SemanticSegmentSegmenter::SemanticSegmentSegmenter(float windowSize, float minSegmentSize, float divideThreshold, float derivation, float weightRoot, float gaussFilterWidth) :
        windowSize(windowSize),
        minSegmentSize(minSegmentSize),
        divideThreshold(divideThreshold),
        derivation(derivation),
        weightRoot(weightRoot),
        gaussFilterWidth(gaussFilterWidth)
    {

    }

    void SemanticSegmentSegmenter::setMotion(MMM::MotionPtr motion)
    {
        motions.clear();
        ModelBasedTaskSegmentation::MotionData data;
        data.motion = motion;
        motions[motion->getName()] = data;
    }

    void SemanticSegmentSegmenter::setMotions(ModelBasedTaskSegmentation::MotionDataMap motions)
    {
        this->motions = motions;
    }

    void SemanticSegmentSegmenter::setMotions(const MMM::MotionList &motions)
    {
        this->motions.clear();
        for(MotionPtr m : motions)
        {
            ModelBasedTaskSegmentation::MotionData data;
            data.motion = m;
            this->motions[m->getName()] = data;
        }
    }

    void SemanticSegmentSegmenter::setWorldStates(memoryx::SECKeyFrameMap worldStates)
    {
        this->worldStates = worldStates;
    }
    void SemanticSegmentSegmenter::setSemanticKeyframes(const NameIntSetMap &secKeyframesByObject)
    {
        this->secKeyframesByObject = secKeyframesByObject;
    }

    void SemanticSegmentSegmenter::calcKeyframes()
    {

        ModelBasedTaskSegmentation::MotionDataMap::iterator it = motions.begin();

        for(; it != motions.end(); it++)
        {
            MMM::MotionPtr motion = it->second.motion;
            SampledTrajectoryV2 traj = MMMConverter::fromMMM(motion, true);
            const string objName = motion->getName();
            std::string path = "plots/inputtraj_" + objName;
//            traj.plotAllDimensions(0,path.c_str());
            traj.gaussianFilter(gaussFilterWidth);
            ARMARX_IMPORTANT_S << "Object Name: " << objName;
//            traj.plotAllDimensions(0,(path+"_filtered").c_str());
            secKeyframesByObject[objName].insert(0);
            secKeyframesByObject[objName].insert(motion->getNumFrames());
            DMP::SemanticSegmentData& semanticSegments = segData[objName];
            semanticSegments.mainObject = objName;

            std::set<int>::const_iterator itKF = secKeyframesByObject[objName].begin();
            size_t secKeyframeNumber = 0;            
            int prevKeyframe = *itKF;
            itKF++;
            for(; itKF != secKeyframesByObject[objName].end(); itKF++, secKeyframeNumber++)
            {
                CharacteristicSegmentationData data;
                double timestamp = prevKeyframe;
                timestamp /= FPS;
                std::cout << "set arrow from " << timestamp << ",graph(0.8,0.8) to " << timestamp << " ,graph(1,1) nohead ls 6;" << std::endl;
                data.actionLabels[timestamp] = "";
//                ARMARX_VERBOSE_S << "Keyframes: " << (timestamp) << " to " << ((double)(*itKF)/FPS);
                SampledTrajectoryV2 subTraj = traj.getPart(timestamp, (double)(*itKF)/FPS, 2);
//                subTraj.plotAllDimensions(0,"plots/inputtraj");
//                subTraj.plotAllDimensions(2,"plots/inputtraj2");
//                double duration = (double)(*itKF)/FPS - (double)(prevKeyframe)/FPS;
//                std::cout << "subtrajetory size: " << subTraj.size() << " dim: " << subTraj.dim() << std::endl;
                CharacteristicSegmentationOnPerturbationTerm segm(subTraj, minSegmentSize, divideThreshold, derivation, windowSize, weightRoot);
                segm.calc();
                segm.plot(CharacteristicSegmentationOnPerturbationTerm::eSVG, /*&subTraj*/NULL);
                DVec newKeyframes = segm.getKeyframes();
                std::vector<int> &newAbsoluteKeyframes = subKeyframes[std::make_pair(it->second.motion->getName(),secKeyframeNumber)];
                newAbsoluteKeyframes.resize(newKeyframes.size());
                std::stringstream str;
                for(size_t f = 0; f < newKeyframes.size(); f++)
                {
                    //                    newAbsoluteKeyframes.at(f) = prevKeyframe + newKeyframes.at(f)/ ( *itKF - prevKeyframe );
                    newAbsoluteKeyframes.at(f) = newKeyframes.at(f) * FPS;
                    str << newAbsoluteKeyframes.at(f) << ", ";
                    std::cout << "set arrow from " << newAbsoluteKeyframes.at(f)*0.01 << ",graph(0.8,0.8) to " << newAbsoluteKeyframes.at(f)*0.01 << " ,graph(1,1) nohead ls 6;" << std::endl;
                    data.actionLabels[newKeyframes.at(f)] = "";
                }
                data.mainObject = objName;
                data.keyframesWithConfidence = segm.getKeyframesWithConfidence();
                data.trajectorySegment = subTraj;
//                ARMARX_VERBOSE_S<< "subKeyframes " << str.str() << std::endl;
                prevKeyframe = *itKF;
                semanticSegments.segments.push_back(data);
            }
        }

    }

    const std::map<string, SemanticSegmentData> &SemanticSegmentSegmenter::getCompleteSegmentationData() const
    {
        return segData;
    }

    memoryx::SECKeyFrameMap SemanticSegmentSegmenter::convertToFlatSegmentation() const
    {
        memoryx::SECKeyFrameMap result;
        for(const auto& segmentPair : segData)
        {
            const SemanticSegmentData& segment = segmentPair.second;
            SemanticSegmentData flatSegment;
            flatSegment.mainObject = segment.mainObject;
            for(const auto& motioncharacteristicsegment : segment.segments)
            {
                int kfIndex = int(motioncharacteristicsegment.actionLabels.begin()->first*100);
                memoryx::SECKeyFrameBasePtr curWS;
                if(worldStates.count(kfIndex))
                    curWS = worldStates.at(kfIndex);
                else
                {
                    auto it = worldStates.lower_bound(kfIndex-1);
                    if(fabs(it->first - kfIndex) < 1)
                        curWS = it->second;
                    else
                        throw LocalException("Could not find matching keyframe:") << kfIndex << " vs. closest " << it->first;
                }
                for(const auto& subkf : motioncharacteristicsegment.actionLabels)
                {
                    result[int(subkf.first*100)] = curWS;
                }
            }

        }

        return result;
    }

    std::map<std::string, std::vector<double> > SemanticSegmentSegmenter::getAllKeyFrames() const
    {
        std::map<std::string, std::vector<double> > result;
        for(auto& elem : segData)
        {
            std::vector<double> objKeyframes;
            DMP::SemanticSegmentData segData = elem.second;
            for(CharacteristicSegmentationData& segment: segData.segments)
            {
                for(auto& label : segment.actionLabels)
                    objKeyframes.push_back(label.first);
            }
            result[elem.first] = objKeyframes;
        }
        return result;
    }


}
